# To try this service

### You can run these commands from command line or terminal
### For making new request just use this command with your values
curl -X POST localhost:8080/requests -H "Content-type:application/json" -d {\"routeNumber\":\"{1439}\",\"routeDate\":\"{2020.09.12,06:30PM}\"}

### To see all requests, use this command or tap on the link to open in browser
curl http://localhost:8080/requests
