package com.example.test_task.entity;

import lombok.*;
import javax.persistence.*;

@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "request")
@ToString(of = {"id", "routeNumber", "routeDate"})
public class Request {

    @Id
    @GeneratedValue
    @NonNull
    private Long id;

    @NonNull
    private String routeNumber;

    @NonNull
    private String routeDate;

    @NonNull
    private String status;
}