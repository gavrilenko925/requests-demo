package com.example.test_task.controller;

import com.example.test_task.dao.RequestRepository;
import com.example.test_task.entity.Request;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Random;

@RestController
public class RequestRestController {

    private final RequestRepository requestRepository;

    public RequestRestController(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    @GetMapping("/requests")
    public List<Request> retrieveAllRequests() {
        return requestRepository.findAll();
    }

    @PostMapping("/requests")
    public @lombok.NonNull String createRequest(@RequestBody Request request) {
        Request newRequest = requestRepository.save(request);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(newRequest.getId()).toUri();
        ResponseEntity.created(location).build();
        final String[] randStatus = {"processed", "error", "completed"};
        Random random = new Random();
        int index = random.nextInt(randStatus.length);
        request.setStatus(randStatus[index]);
        return request.getStatus();
    }
}

//curl -X POST localhost:8080/requests -H "Content-type:application/json" -d {\"routeNumber\":\"{1439}\",\"routeDate\":\"{2020.09.12,06:30PM}\"}